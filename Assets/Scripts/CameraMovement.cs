﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject cameraFocus;

    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - cameraFocus.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = cameraFocus.transform.position + offset;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PersonManager : MonoBehaviour {
    [SerializeField]
    GameObject preFab;

    [SerializeField,Range(6,10)]
    int startingNumberOfPeople;

    int currentPeopleCount;

    public List<Person> people = new List<Person>();

	// Use this for initialization
	void Start () {
        AddPeople();
	}

    void AddPeople()
    {
        for (int i = 0; i < startingNumberOfPeople; i++)
        {
            Vector3 startingPos = new Vector3(Random.Range(-transform.localScale.x * 4f, transform.localScale.x * 4f), 0f, Random.Range(-transform.localScale.z * 4f, transform.localScale.z * 4f));
            GameObject person = GameObject.Instantiate(preFab, startingPos,Quaternion.identity);
            person.transform.SetParent(transform);
            Person personScript = person.GetComponent<Person>();
            personScript.sphereCollider = GetComponent<SphereCollider>();
            personScript.id = currentPeopleCount;
            people.Add(personScript);
            currentPeopleCount++;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactible : MonoBehaviour {
    public List<Person> peopleMovingToInteractionPoint = new List<Person>();
    public virtual void Interact(Person person)
    {
        if(person.inter != null)
        {
            person.inter.peopleMovingToInteractionPoint.Remove(person);
        }
        //Use this for interactions
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

    Dictionary<ResourceType, int> resourceManager = new Dictionary<ResourceType, int>();

    public int GetResourceManager (ResourceType resourceType)
    {
        if (!resourceManager.ContainsKey(resourceType))
        {
            resourceManager.Add(resourceType, 0);
        }
        return resourceManager[resourceType];
    }

    public bool SetResourceManager(ResourceType resourceType, int adjustResource)
    {
        if (adjustResource + GetResourceManager(resourceType) >= 0)
        {
            resourceManager[resourceType] += adjustResource;
            return true;
        }
        else return false;
    }
}
public enum ResourceType{
    wood,
    food,
    stone,
    skin
}
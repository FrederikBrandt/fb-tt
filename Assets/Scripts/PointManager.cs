﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointManager : MonoBehaviour {

    [SerializeField]
    GameObject mapPlane;

    [SerializeField]
    GameObject campArea;
    
    [SerializeField]
    GameObject[] preFab;

    [SerializeField]
    Transform parent;

    [SerializeField]
    int[] numberToSpawnMax;

    [SerializeField]
    int[] numberToSpawnMin;

    List<ResourcePoint> points = new List<ResourcePoint>();

    public Renderer rend;
    float pointAreaX;
    float pointAreaZ;
    Vector3 center;




    // Use this for initialization
    void Start()
    {
        // Generates the max and min coordinates, to make sure they spawn on the MapPlane
        rend = mapPlane.GetComponent<Renderer>();
        pointAreaX = rend.bounds.size.x / 2;
        pointAreaZ = rend.bounds.size.z / 2;
        center = rend.bounds.center;
        if (parent == null)
        {
            parent = GameObject.Instantiate(new GameObject("Resources")).transform;
            parent.SetParent(transform);
        } 
        // Initiates creating resourcePoints
        CreatePoints();
    }



    void CreatePoints()
    {
        // Counts number of preFabs defined on gameobject manager
        for (int x = 0; x < preFab.Length; x++)
        {
            // Starts generating resource points, based upon each prefabs values
            for (int i = 0; i < Random.Range(numberToSpawnMin[x], numberToSpawnMax[x]); i++)
            {
                // Randomly generates coordinates on the plane
                float targetCoordsX = center.x + Random.Range(-pointAreaX, pointAreaX);
                float targetCoordsZ = center.z + Random.Range(-pointAreaZ, pointAreaZ);
                Vector3 pos = new Vector3 (targetCoordsX,0f, targetCoordsZ);

                // Creates an instance of the preFab
                GameObject go = GameObject.Instantiate(preFab[x], pos,Quaternion.identity);
                ResourcePoint rp = go.GetComponent<ResourcePoint>();

                // Creates resources associated with this instance
                rp.SpawnResourcePoint(pos);

                // ?
                points.Add(rp);

                // Moves resourcepoints under Gameobject resources
                go.transform.SetParent(parent);
            }
        }
    }
}
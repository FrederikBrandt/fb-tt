﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour {
    public ResourceType[] resourceType;
    public int[] amount;
    public Dictionary<ResourceType, int> resources = new Dictionary<ResourceType, int>();

    private void Start()
    {
        for (int i = 0; i < amount.Length; i++)
        {
            resources.Add(resourceType[i], amount[i]);
        }
    }
}

// ResourcePoint quality, Resource respawn rate, Resource Max, Resource Respawn

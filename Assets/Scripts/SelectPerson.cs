﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPerson : MonoBehaviour {

	[SerializeField]
	Camera cam;

	Ray ray;
	

	public Person selectedPerson;

	void Start () {
			
	}
	
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			if(selectedPerson == null)
			{ 
			    ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.nearClipPlane));
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
			    {
                    if (hit.collider.CompareTag("Person"))
				    {
					    selectedPerson = hit.collider.GetComponentInParent<Person>();
				    }
			    }
			}
            else
            {
                ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.nearClipPlane));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.CompareTag("Interactible"))
                    {
                        hit.collider.GetComponent<Interactible>().Interact(selectedPerson);
                        selectedPerson = null;
                    }
                    else
                    {
                        selectedPerson = null;
                    }
                }
            }
		}
	}
}

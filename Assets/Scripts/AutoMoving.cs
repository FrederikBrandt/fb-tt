﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AutoMoving : MonoBehaviour
{

    public Vector3 destination;
    public SphereCollider sphereCollider;
    NavMeshAgent agent;
    public float secondsToWaitAverage;
    float waitingSeconds;
    bool waitingForNewRandom = true;


    // Use this for initialization
    void Start()
    {
        sphereCollider = GetComponentInParent<SphereCollider>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (destination == Vector3.zero)
        {
            if (!waitingForNewRandom)
            {
                if (agent.velocity.magnitude < 0.005f)
                {
                    waitingForNewRandom = true;
                    waitingSeconds = Time.time + secondsToWaitAverage + Random.Range(-3f, 3f);
                }
            }
            else if (waitingForNewRandom && Time.time > waitingSeconds && agent.velocity.magnitude < 0.005f)
            {
                waitingForNewRandom = false;
                agent.destination = GetMovement(sphereCollider);
            }
        }
    }

    public Vector3 GetMovement(SphereCollider colliderRadius)
    {
        float radius = colliderRadius.radius * 2;
        destination = Vector3.zero;
        return new Vector3(Random.Range(-radius, radius), 0f, Random.Range(-radius, radius)) + colliderRadius.transform.position;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Highlight : MonoBehaviour {

    GameObject halo;
    int times;
    bool activeOrNot = true;
    bool pathReached;
    NavMeshAgent agent;

    [SerializeField]
    Camera cam;

	// Use this for initialization
	void Start () {
        halo = transform.Find("Halo").gameObject;
        ToggleHalo();

        agent = GetComponent<NavMeshAgent>();
    }

    public void OnMouseDown()
    {
        ToggleHalo();
    }

    public void ToggleHalo()
    {
        activeOrNot = !activeOrNot;
        halo.SetActive(activeOrNot);
    }

   /* public void Unpack() {
    }*/

    private void FixedUpdate()
    {
        if (activeOrNot)
        {
            if (Input.GetMouseButtonDown(0))
            {
                agent.destination = cam.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 10);
                Debug.Log(Input.mousePosition);
            }
        }

      /*  if (pathReached)
        {
            Unpack();
        }*/
    }
}

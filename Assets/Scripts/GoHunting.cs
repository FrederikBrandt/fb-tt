﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoHunting : Interactible {
    [SerializeField]
    float cooldown;

    Queue<Person> peopleAway = new Queue<Person>();
    Queue<float> timeToReturn = new Queue<float>();

    public override void Interact(Person person)
    {
        base.Interact(person);
        peopleMovingToInteractionPoint.Add(person);
        person.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(transform.position);
    }

    private void Update()
    {
        if(timeToReturn.Count > 0)
        {
            if(timeToReturn.Peek() < Time.time)
            {
                peopleAway.Dequeue().gameObject.SetActive(true);
                timeToReturn.Dequeue();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Person person = other.GetComponentInParent<Person>();
        if (person == null) return;
        bool personFound = false;
        foreach( Person p in peopleMovingToInteractionPoint)
        {
            if(person.id == p.id)
            {
                
                peopleAway.Enqueue(person);
                timeToReturn.Enqueue(Time.time + cooldown);
                person.gameObject.SetActive(false);
                personFound = true;
                //Skal først allokeres her. Det hele burde være i interactible scriptet.
            }
        }
        if(personFound)peopleMovingToInteractionPoint.Remove(person);
    }
}

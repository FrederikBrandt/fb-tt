﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePoint : MonoBehaviour
{

    [SerializeField]
    GameObject preFab;

    [SerializeField]
    int numberToSpawnMax;

    [SerializeField]
    int numberToSpawnMin;

    [SerializeField]
    int respawnRate;

    [SerializeField]
    int maxResources;

    [SerializeField]
    int populationDensity;

    float timer;
    float coolDown = 10;
    public QualityType qualityType;


    List<Resource> resources = new List<Resource>();

    private void Start()
    {
        timer = Time.time;
    }

    public void CreateResources(Vector3 pos, int i)
    {
        resources.Add(GameObject.Instantiate(preFab, pos,Quaternion.identity).GetComponent<Resource>());
        resources[i].transform.SetParent(transform);
    }

    public void SpawnResourcePoint(Vector3 pos)
    {
        int qualityTypeInt = Random.Range(0, System.Enum.GetNames(typeof(QualityType)).Length);

        qualityType = (QualityType)qualityTypeInt;

        maxResources = (qualityTypeInt + 1) * 2 * populationDensity;

        for (int i = 0; i < Random.Range(numberToSpawnMin, numberToSpawnMax); i++)
        {
            CreateResources(pos, i);
        }
    }

    private void Update()
    {
        if (Time.time - timer > coolDown)
        {
            int currentResources = resources.Count;

            if (currentResources < maxResources)
            {
                double parents = currentResources * 0.5;
                float children = (float)parents * respawnRate * Random.Range(0, 0.51f);
                Vector3 pos = transform.position;

                int overPopulation = currentResources + (int)children - maxResources;

                if (overPopulation > 0)
                {
                    children = children - overPopulation;
                }

                for (int i = 0; i < children; i++)
                {
                    CreateResources(pos, i);
                }
            }

            timer = Time.time + Random.Range(-30, 30);
        }
    }
}


public enum QualityType
{
    poor,
    normal,
    sublime
}